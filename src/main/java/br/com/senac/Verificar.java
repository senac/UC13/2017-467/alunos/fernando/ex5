package br.com.senac;

public class Verificar {

    public boolean isPar(int numero) {
        return numero % 2 == 0;
    }

    public boolean isPositivo(int numero) {
        return numero >= 0;
    }
}
