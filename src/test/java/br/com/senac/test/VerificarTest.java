package br.com.senac.test;

import br.com.senac.Verificar;
import org.junit.Test;
import static org.junit.Assert.*;

public class VerificarTest {

    public VerificarTest() {
    }

    @Test
    public void deveSerPar() {
        Verificar verificar = new Verificar();
        int numero = 10;
        boolean resultado = verificar.isPar(numero);
        assertTrue(resultado);
    }
    
    @Test
    public void naoDeveSerPar(){
        Verificar verificar = new Verificar();
        int numero = 5;
        boolean resultado = verificar.isPar(numero);
        assertFalse(resultado);
    }

    @Test
    public void deveSerPositivo() {
        Verificar verificar = new Verificar();
        int numero = 10;
        boolean resultado = verificar.isPositivo(numero);
        assertTrue(resultado);
    }
    
    @Test
    public void naoDeveSerPositivo(){
        Verificar verificar = new Verificar();
        int numero = -5;
        boolean resultado = verificar.isPositivo(numero);
        assertFalse(resultado);
    }
}
